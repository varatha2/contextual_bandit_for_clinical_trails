function [all_regrets, all_subopts, action_cts] = mab_general(cb_struct_curr, n_patients)

    uniq_actions = unique(cb_struct_curr.actions);

    priors = zeros(1, length(uniq_actions));

    for i = 1 : length(uniq_actions)
        priors(i) = sum(cb_struct_curr.rewards(cb_struct_curr.actions == uniq_actions(i))) / sum(cb_struct_curr.actions == uniq_actions(i));
    end
    
    % priors = priors / sum(priors);
    
    
    % Scenario of Figure 1 in [Kaufmann, Cappé & Garivier, AISTATS 2012]
    game = gameBernoulli(priors); fname = '../maBandits_results/general';
    % Choice of policies to be run
%     policies = {policyRandom(), policyBayesUCB(), policyThompson(), policyKLUCB(), ...
%                 policyCPUCB(), policyUCB(), policyUCBtuned(), policyUCBV(), policyMOSS(), ...
%                 policyDMED(), policyKLempUCB};
    policies = {policyRandom(), policyUCB(), policyThompson()};
    % n is length of play, N is number of plays 
    n = n_patients; N = 20;
    % Time indices at which the results will be saved
    tsave = round(linspace(1,n,200));

    % Run everything one policy after each other
    defaultStream = RandStream.getGlobalStream; 
    savedState = defaultStream.State;
    for k = 1:length(policies)
        defaultStream.State = savedState;
        tic; 
        [all_regrets{k}, all_subopts{k}, action_cts{k}] = experiment_context_free(game, n, N, policies{k}, tsave, fname); 
        toc; 
    end

    % experiment(game, n, N, policy_curr, tsave, fname);
    % plotResults(game, n, N, {policy_curr}, fname);

    % [all_regrets, all_subopts] = plotResults(game, n, N, policies, fname);
    
end