clc;
clear all;

load('cb_struct_str.mat');

%% new code - IST data
addpath('Bandits/');
    
[all_regrets, all_subopts, all_action_cts] = mab_general(cb_struct_str, length(cb_struct_str.rewards));

[all_regrets_c, all_subopts_c, all_action_cts_c] = mab_contextual_3(cb_struct_str, length(cb_struct_str.rewards));

figure;
plot_all_results(all_regrets, all_subopts, length(cb_struct_str.states));
figure;
plot_all_results(all_regrets_c, all_subopts_c, length(cb_struct_str.states));