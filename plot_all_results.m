function [] = plot_all_results(all_regrets, all_subopts, n_patients)

    l = 3;
    tsave = round(linspace(1,n_patients,200));
    policies = {policyRandom(), policyUCB(), policyThompson()};

    clf;

    % Common y-axis values
    ar = 1400;
    ad = 2;
    risk1 = 25; 

    for i = 1:l

        policyName = class(policies{i}); policyName = policyName(7:end);

        % Regret plot
        regret = all_regrets{i};
        % regret = regret / length(cb_struct_str.states);
        subplot(2,l,i);
        h = area(tsave, [prctile(regret, risk1); prctile(regret, 100-risk1) - prctile(regret, risk1)]');
        set(h(1),'Visible', 'off');
        set(h(2),'FaceColor', [0.86 0.86 0.86]);
        hold on;  
        h = plot(tsave, mean(regret), 'k');
        linewidth = 3; lineCover = 3*linewidth;
        set(h, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
        if (i == 1), ylabel('Regret(R)'); end
        if (i == 2), xlabel('# participants'); end
        title(policyName);
        set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);
        tmp = axis; ar = max(ar, tmp(4));

        % Suboptimal draws plot
        subplot(2,l,l+i);
        subOpt = all_subopts{i} / 10000;
        h = area(tsave, [prctile(subOpt, risk1); prctile(subOpt, 100-risk1) - prctile(subOpt, risk1)]');
        set(h(1),'Visible', 'off');
        set(h(2),'FaceColor', [0.86 0.86 0.86]);
        hold on;
        h = plot(tsave, mean(subOpt), 'k');
        linewidth = 3; lineCover = 3*linewidth;
        set(h, 'LineWidth', linewidth, 'Marker', '.', 'MarkerSize', lineCover);
        if (i == 1), ylabel('Suboptimal draw probability(D)'); end
        if (i == 2), xlabel('# participants'); end
        title(policyName);
        set(gca, 'fontsize', 20, 'FontWeight', 'bold', 'linewidth', 2);
        tmp = axis; ar = max(ar, tmp(4));

    end
    % Fix comparable y-axis
    for i = 1:l
      subplot(2,l,i);
      axis([0 tsave(end) 0 ar]);
      subplot(2,l,l+i);
      axis([0 tsave(end) 0 ad]);
    end
end
   