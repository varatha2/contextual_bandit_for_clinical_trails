function [all_regrets, all_subopts, action_cts] = mab_contextual_3(cb_struct_curr, n_patients)

%     clc;
%     clear all;
%     load('cb_struct_str.mat');
%     cb_struct_curr = cb_struct_str;
%     n_patients = length(cb_struct_curr.rewards);

    uniq_states = unique(cb_struct_curr.states);
    
    uniq_actions = unique(cb_struct_curr.actions);
    
    state_priors = zeros(1, length(uniq_states));

    action_priors = zeros(length(uniq_states), length(uniq_actions));
    
    for st = 1 : length(uniq_states)
        curr_actions = cb_struct_curr.actions(cb_struct_curr.states == uniq_states(st));
        curr_rewards = cb_struct_curr.rewards(cb_struct_curr.states == uniq_states(st));
        
        state_priors(st) = sum(cb_struct_curr.states == uniq_states(st)) / length(cb_struct_curr.states);

        for i = 1 : length(uniq_actions)
            if(sum(curr_actions == uniq_actions(i)) ~= 0)
                action_priors(st, i) = sum(curr_rewards(curr_actions == uniq_actions(i))) / sum(curr_actions == uniq_actions(i));
            end
        end
        % action_priors(st, :) = action_priors(st, :) / sum(action_priors(st, :));
        
        games{st} = gameBernoulli(action_priors(st, :));

    end
    
    fname = '../cmaBandits_results/context_';
    
%     Choice of policies to be run
%     policies = {policyRandom(), policyBayesUCB(), policyThompson(), policyKLUCB(), ...
%                 policyCPUCB(), policyUCB(), policyUCBtuned(), policyUCBV(), policyMOSS(), ...
%                 policyDMED(), policyKLempUCB};

    policies = {policyRandom(), policyUCB(), policyThompson()};
    % n is length of play, N is number of plays 
    n = n_patients; N = 20;
    % Time indices at which the results will be saved
    tsave = round(linspace(1,n,200));

    % Run everything one policy after each other
    defaultStream = RandStream.getGlobalStream; 
    savedState = defaultStream.State;
    for k = 1:length(policies)
        defaultStream.State = savedState;
        tic; 
        [all_regrets{k}, all_subopts{k}, action_cts{k}] = experiment_contextual(games, cb_struct_curr.states, state_priors, N, policies{k}, tsave, fname); 
        toc; 
    end
    
    % [all_regrets, all_subopts] = plotResultsContextual(n, N, policies, fname);
    
end