load('IST_corrected.mat');

%% remove rows with at least one empty entry
col_names = {'SEX', 'AGE', 'RATRIAL', 'RXASP', 'RXHEP', 'DOAC', 'DALIVE', 'DRSISC', 'DTHROMB', 'FDEAD'};
invalid_idx = zeros(size(ISTcorrected, 1), 1);

for i = 1 : length(col_names)
    invalid_idx(strcmp(eval(strcat('ISTcorrected.', col_names{i})), '')) = 1;
end

%%
ISTcorrected(invalid_idx | (strcmp(ISTcorrected.DTHROMB, 'Y')), :) = [];

states_bin = strcmp(ISTcorrected.RATRIAL, 'Y');
actions_bin = [strcmp(ISTcorrected.RXHEP, 'M') | strcmp(ISTcorrected.RXHEP, 'H'), strcmp(ISTcorrected.RXASP, 'Y'), strcmp(ISTcorrected.DOAC, 'Y')];

cb_struct_str.states = bi2de(states_bin);
cb_struct_str.state_names = {'RATRIAL'};
cb_struct_str.actions = bi2de(actions_bin);
cb_struct_str.action_names = {'RXHEP', 'RXASP', 'DOAC'};
cb_struct_str.rewards = strcmp(ISTcorrected.DRSISC, 'Y');

%%
save('cb_struct_str.mat', 'cb_struct_str');