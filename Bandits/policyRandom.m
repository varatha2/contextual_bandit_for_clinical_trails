classdef policyRandom < Policy
    % Thompson policy for Bernoulli bandits
    %
    % authors: Olivier Cappé, Aurélien Garivier and Emilie Kaufmann
    % Ref.: [Kaufmann, Korda & Munos, arXiv:1205.4217]

    % $Id: policyThompson.m,v 1.5 2012-06-05 13:26:38 cappe Exp $
    
    properties
        t % Number of round
        lastAction % Stores the last action played
        N % Nb of times each action has been chosen
        S % Cumulated reward with each action
    end
    
    methods
        function self = policyRandom()
            
        end
        function init(self, nrActions, horizon)
            self.t = 1;
            self.N = zeros(1, nrActions);
            self.S = zeros(1, nrActions);
        end
        function action = decision(self)
            % Draws from posteriors
            action = randi(length(self.S));
            self.lastAction = action;
        end
        function getReward(self, reward)
            self.N(self.lastAction) = self.N(self.lastAction) + 1; 
            self.S(self.lastAction) = self.S(self.lastAction) + reward;
            self.t = self.t + 1;
        end        
    end

end