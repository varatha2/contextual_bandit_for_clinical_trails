function [regrets, subOpts, action_cts] = experiment_contextual(games, states, state_priors, N, policy, tsave, fname)
    % Run a number of experiments and save the results subsampling in time
    % Use: experiment(game, n, N, policy, tsave, fname),
    % n is the horizon and N the number of played games.
    %
    % authors: Olivier Cappé and Aurélien Garivier

    % $Id: experiment.m,v 1.17 2012-06-06 09:42:04 cappe Exp $
    
    if nargin<5, tsave = 1:n; end % Times at which the results are saved
    if nargin<6, fname = 'results/exp'; end % Defaut file name
    K = length(tsave); % tsave contains the time indices at which the
                       % results will be saved
    
    % Cumulated rewards: N repeats x K subsanpled times between 1 and n
    cumReward = zeros(N, K);
    regrets = zeros(N, K);
    % Number of times each arm has been played: N repeats x K subsanpled
    % times between 1 and n x number of arms
    cumNbPlayed = zeros(N, K, games{1}.nbActions);
    subOpts = ones(N, 1) * tsave;
    
    action_cts = zeros(N, length(games{1}.mu));
    
    fprintf('%s %d:', class(policy), N);
    for j = 1:N, 
        for g = 1 : length(games)
            game = games{g};
            game.initPolicy(policy, length(states));
        end
        rewards = zeros(1, length(states));
        actions = zeros(1, length(states));
        
        rewards_opt = zeros(1, length(states));
        actions_opt = zeros(1, length(states));
        
        for st = 1 : length(states)
            game = games{states(st) + 1};
            [rewards(st), actions(st)] = game.play(1);
            [rewards_opt(st), actions_opt(st)] = max(game.mu); 
            
            rewards(st) = rewards(st) * state_priors(states(st) + 1);
            rewards_opt(st) = rewards_opt(st) * state_priors(states(st) + 1);
        end
        cr = cumsum(rewards);
        cro = cumsum(rewards_opt);
        cumReward(j, :) = cr(tsave);
        regrets(j, :) = (cro(tsave) - cr(tsave));
        for a = 1 : games{1}.nbActions
            action_cts(j, a) = sum(actions == a);
            
            ca = cumsum(actions == a);
            cumNbPlayed(j, :, a) = ca(tsave);
            
            subo = cumsum(actions_opt == a & actions == a);
            subOpts(j, :) = subOpts(j, :) - subo(tsave);
        end
        % Once every N/50 runs, display something and save current state
        % of variables
        if (rem(j, floor(N/50))==0) | (j == N)
            fprintf(' %d', j);
            % Expectations of the arms
%             save([fname '_n_' num2str(length(states)) '_N_' num2str(N) '_' class(policy)],...
%               'regrets', 'subOpts', 'tsave', 'cumReward', 'cumNbPlayed');
        end
    end
    fprintf('\n');   
end
